# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) All rights reserved:
#        (c) 2014      Anubía, soluciones en la nube,SL (http://www.anubia.es)
#                      Alejandro Santana <alejandrosantana@anubia.es>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses
#
##############################################################################
{
    "name": "Unit of measure symbols",
    "version": "1.0",
    "depends": ["base"],
    "author": "Alejandro Santana <alejandrosantana@anubia.es>",
    "category": "Product",
    "description": """
Unit of measure symbols
-----------------------
* This modules adds a 'description' field to product 'unit of measure' object.
  Thus it is possible to use/print its long translatable name ('description')
  or its symbol (already existent field 'name', for compatibility).
* Also, provides definition of some categories and their usual units, with
  their symbols following the International System of Units (SI).

NOTES:
------
* Some ids have been changed, but the uom that comes with standard
  OpenERP are not deleted nor deactivated. In case you do not want this
  duplicates, you should delete them manually.
* Some records have been redefined, so it may change your values if you
  modified them.

""",
    "init_xml": [],
    'update_xml': ['product.xml'],
    'demo_xml': ['data/units_si.xml'],
    'installable': True,
    'active': False,
#    'certificate': 'certificate',
}