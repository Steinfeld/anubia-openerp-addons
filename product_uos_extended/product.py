# -*- coding: utf-8 -*-
################################################################
#    License, author and contributors information in:          #
#    __openerp__.py file at the root folder of this module.    #
################################################################

import decimal_precision as dp
from osv import osv, fields
from tools.translate import _


class product_uom(osv.osv):
    _inherit = 'product.uom'

    def _qty_swap_uom_uos(self, cr, uid,
                          product_id=None, swap_type='uom2uos',
                          uom1_id=False, uom1_qty=False,
                          uom2_id=False, context=None):
        """
        Computes the quantity of a product between:
            uom (or same category) <--> uos (or same category)
            a) swap_type='uom2uos': from a uom to a uos.
            b) swap_type='uos2uom': from a uos to a uom.
            NOTE: swap_type: ('uom2uos', 'uos2uom')
        Internally makes three steps:
            1) Calculates qty(from_uom1) -> qty(default_uom1)
            2) Calculates qty(default_uom1) -> qty(default_uom2) with uos_coeff
            3) Calculates qty(default_uom2) -> qty(to_uom2)
            NOTE: default_uom1 and default_uom2 are product.template uom,uos
        """
        if not product_id or not uom1_id or not uom1_qty or not uom2_id:
            return uom1_qty

        product_obj = self.pool.get('product.product')
        product = product_obj.browse(cr, uid, product_id, context=context)
        if not product or not product.uos_id or not product.uos_coeff:
            return uom1_qty

        def_uom_id = product.uom_id.id
        uos_coeff = product.uos_coeff
        def_uos_id = product.uos_id.id

        if swap_type == 'uom2uos':
            def_uom1_id = product.uom_id.id
            def_uom2_id = product.uos_id.id
            coeff = product.uos_coeff
        elif swap_type == 'uos2uom':
            def_uom1_id = product.uos_id.id
            def_uom2_id = product.uom_id.id
            try:
                coeff = 1 / product.uos_coeff
            except ZeroDivisionError:
                return uom1_qty

        product_uom_obj = self.pool.get('product.uom')
        def_uom1_qty = self._compute_qty(cr, uid,
                                         uom1_id, uom1_qty, def_uom1_id)
        def_uom2_qty = def_uom1_qty * coeff
        uom2_qty = product_uom_obj._compute_qty(cr, uid,
                                                def_uom2_id,
                                                def_uom2_qty,
                                                uom2_id)
        return uom2_qty

    def _price_swap_uom_uos(self, cr, uid,
                            product_id=None, swap_type='uom2uos',
                            uom1_id=False, uom1_price=False,
                            uom2_id=False, context=None):
        """
        Computes the quantity of a product:
            a) swap_type='uom2uos': from a uom to a uos.
            b) swap_type='uos2uom': from a uos to a uom.
            NOTE: swap_type: ('uom2uos', 'uos2uom')
        Internally makes three steps:
            1) Calculates price(from_uom1) -> price(default_uom1)
            2) Calculates price(def_uom1) -> price(def_uom2) with uos_coeff
            3) Calculates price(def_uom2) -> price(to_uom2)
            NOTE: default_uom1 and default_uom2 are product.template uom,uos
        """
        if not product_id or not uom1_id or not uom1_price or not uom2_id:
            return uom1_price
        if not swap_type or swap_type not in ('uom2uos', 'uos2uom'):
            return uom1_price

        product_obj = self.pool.get('product.product')
        product = product_obj.browse(cr, uid, product_id, context=context)

        if not product or not product.uos_id or not product.uos_coeff:
            return uom1_price

        if swap_type == 'uom2uos':
            def_uom1_id = product.uom_id.id
            def_uom2_id = product.uos_id.id
            try:
                coeff = 1 / product.uos_coeff
            except ZeroDivisionError:
                return uom1_price
        elif swap_type == 'uos2uom':
            def_uom1_id = product.uos_id.id
            def_uom2_id = product.uom_id.id
            coeff = product.uos_coeff

        product_uom_obj = self.pool.get('product.uom')
        # From uom1 to def_uom1
        def_uom1_price = product_uom_obj._compute_price(cr, uid,
                                                        uom1_id, uom1_price,
                                                        def_uom1_id)
        # From def_uom1 to def_uom2
        def_uom2_price = def_uom1_price * coeff
        # From def_uom2 to uom2
        uom2_price = product_uom_obj._compute_price(cr, uid,
                                                    def_uom2_id,
                                                    def_uom2_price,
                                                    uom2_id)
        return uom2_price

product_uom()


class product_product(osv.osv):
    _inherit = "product.product"
    _columns = {
        'list_price_uos': fields.float(
            'Sale Price per UoS',
            digits_compute=dp.get_precision('Sale Price'),
            help=("Base price (per Unit of Sale, if"
                  " specified) for computing the customer"
                  " price. Sometimes called the catalog"
                  " price."),
            readonly=False),
    }
    _defaults = {
        'list_price_uos': lambda *a: 1.0,
    }

    def onchange_uos(self, cr, uid, ids,
                     uom_id=None, uos_id=None, uos_coeff=1.0,
                     list_price=1.0, list_price_uos=1.0):
        """ Updates list_price or list_price_uos """
        value = {}
        if list_price is None:
            list_price = 1.0
        if not uos_coeff:
            uos_coeff = 1.0
        if not uos_id:
            value = {
                'uos_id': '',
                'uos_coeff': uos_coeff,
                'list_price': list_price,
                'list_price_uos': list_price,
            }
        else:
            if not list_price_uos:
                list_price_uos = list_price  # 1.0
            value = {
                'uos_id': uos_id,
                'uos_coeff': uos_coeff,
                'list_price': list_price_uos * uos_coeff,
                'list_price_uos': list_price_uos,
            }
        return {'value': value}

    def onchange_uos_coeff(self, cr, uid, ids,
                           uom_id=None, uos_id=None, uos_coeff=1.0,
                           list_price=1.0, list_price_uos=1.0):
        """ Updates list_price or list_price_uos """
        value = {}
        if list_price is None:
            list_price = 1.0
        if not uos_id:
            value = {
                'uos_id': None,
                'uos_coeff': 1.0,
                'list_price': list_price,
                'list_price_uos': list_price,
            }
        else:
            if not uos_coeff:
                uos_coeff = 1.0
            if not list_price_uos:
                list_price_uos = 1.0
            value = {
                'uos_coeff': uos_coeff,
                'list_price': list_price_uos * uos_coeff,
                'list_price_uos': list_price_uos,
            }
        return {'value': value}

    def onchange_list_price(self, cr, uid, ids,
                            uom_id=None, uos_id=None, uos_coeff=1.0,
                            list_price=1.0, list_price_uos=1.0):
        """ Updates list_price_uos if needed """
        value = {}
        if list_price is None:
            list_price = 1.0
        if not uos_id:
            value = {
                'uos_id': None,
                'uos_coeff': 1.0,
                'list_price': list_price,
                'list_price_uos': list_price,
            }
        else:
            if not uos_coeff:
                uos_coeff = 1.0
            value = {
                'uos_coeff': uos_coeff,
                'list_price': list_price,
                'list_price_uos': list_price / uos_coeff,
            }
        return {'value': value}

    def onchange_list_price_uos(self, cr, uid, ids,
                                uom_id=None, uos_id=None, uos_coeff=1.0,
                                list_price=1.0, list_price_uos=1.0):
        """ Updates list_price """
        value = {}
        if list_price is None:
            list_price = 1.0
        if not uos_id:
            value = {
                'uos_id': None,
                'uos_coeff': 1.0,
                'list_price': list_price,
                'list_price_uos': list_price,
            }
        else:
            if not uos_coeff:
                uos_coeff = 1.0
            if not list_price_uos:
                list_price_uos = 1.0
            value = {
                'uos_coeff': uos_coeff,
                'list_price': list_price_uos * uos_coeff,
                'list_price_uos': list_price_uos,
            }
        return {'value': value}

product_product()
