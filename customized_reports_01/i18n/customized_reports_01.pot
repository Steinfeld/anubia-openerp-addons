# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* customized_reports_01
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 6.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-03-20 19:37+0000\n"
"PO-Revision-Date: 2014-03-20 19:37+0000\n"
"Last-Translator: Alejandro Santana <alejandrosantana@anubia.es>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: customized_reports_01
#: sql_constraint:purchase.order:0
msgid "Order Reference must be unique per Company!"
msgstr ""

#. module: customized_reports_01
#: model:ir.model,name:customized_reports_01.model_purchase_order_line
msgid "Purchase Order Line"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Non Assigned Products:"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Incoterm:"
msgstr ""

#. module: customized_reports_01
#: model:ir.actions.report.xml,name:customized_reports_01.report_purchase_quotation
msgid "Request for Quotation"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Tel. :"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Shipping Address :"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Date Req."
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "VAT number:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Shipping address:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Invoice address:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
#: report:stock.picking.list:0
msgid "Description"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Contact Address :"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "."
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Discount"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Total :"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Fax :"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Recipient"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Our Order Reference"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Your Order Reference"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Purchase Order Confirmation N°"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Order Date"
msgstr ""

#. module: customized_reports_01
#: model:ir.actions.report.xml,name:customized_reports_01.report_picking_list
msgid "Packing list"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Net Total:"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "["
msgstr ""

#. module: customized_reports_01
#: model:ir.actions.report.xml,name:customized_reports_01.report_purchase_order
#: model:ir.model,name:customized_reports_01.model_purchase_order
msgid "Purchase Order"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "]"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Packing List:"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Quantity"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Taxes :"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Total"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Weight"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "TVA :"
msgstr ""

#. module: customized_reports_01
#: model:ir.model,name:customized_reports_01.model_account_invoice_line
msgid "Invoice Line"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Qty"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "______/______/______"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Unit Price"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Request for Quotation N°"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
msgid "Order(Origin)"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Net Price"
msgstr ""

#. module: customized_reports_01
#: model:ir.model,name:customized_reports_01.model_sale_order_line
msgid "Sales Order Line"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Validated By"
msgstr ""

#. module: customized_reports_01
#: report:stock.picking.list:0
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "VAT number:"
msgstr ""

#. module: customized_reports_01
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "VAT :"
msgstr ""

#. module: customized_reports_01
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "TVA :"
msgstr ""

#. module: sale_early_payment_discount_uos
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Payment Term"
msgstr ""

#. module: sale_early_payment_discount_uos
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Payment Type"
msgstr ""

#. module: sale_early_payment_discount_uos
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Quantity (1)"
msgstr ""

#. module: sale_early_payment_discount_uos
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Quantity (2)"
msgstr ""

#. module: sale_early_payment_discount_uos
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Unit Price (1)"
msgstr ""

#. module: sale_early_payment_discount_uos
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Unit Price (2)"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Your reference"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
#: report:purchase.order:0
#: report:purchase.discount.order:0
msgid "Notes"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Reception"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Comments:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Signature"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Received on:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Carrier info"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Incoterm:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Carrier:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Carrier ref:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Carrier vehicle:"
msgstr ""

#. module: customized_reports_01
#: report:purchase.order:0
#: report:sale.order:0
#: report:stock.picking.list:0
#: report:account.invoice:0
msgid "Carrier notes:"
msgstr ""
